Dataextension name: MASTER_MERGE_DE

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| OLD_CUSTOMER_ID      | Text(50)        | false    | false    |                 |
| NEW_CUSTOMER_ID      | Text(50)        | false    | false    |                 |
| MERGE_DATE           | Text(50)        | false    | false    |                 |
| SEQUENCE_NO          | Text(50)        | true     | true     |                 |
