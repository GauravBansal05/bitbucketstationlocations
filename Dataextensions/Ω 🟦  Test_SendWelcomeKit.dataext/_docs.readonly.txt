Dataextension name: Test_SendWelcomeKit

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| FirstName            | Text(50)        | true     | false    |                 |
| LastName             | Text(50)        | true     | false    |                 |
| Mobile               | Phone           | true     | true     |                 |
| Email                | EmailAddress    | true     | true     |                 |
| City                 | Text(50)        | true     | false    |                 |
