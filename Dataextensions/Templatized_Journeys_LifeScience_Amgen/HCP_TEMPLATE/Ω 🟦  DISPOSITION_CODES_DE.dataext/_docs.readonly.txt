Dataextension name: DISPOSITION_CODES_DE

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| DISPOSITION_CD       | Text(50)        | true     | true     |                 |
| DISPOSITION_DESC     | Text(256)       | true     | false    |                 |
| CHANNEL_CD           | Text(50)        | true     | false    |                 |
| CATEGORY             | Text(256)       | true     | false    |                 |
| IMPRESSION_ENGAGEMENT | Text(256)       | true     | false    |                 |
| ZS_ENGAGER_SCORE     | Text(50)        | true     | false    |                 |
| isContactDisposition | Boolean         | true     | false    | False           |
