Dataextension name: BRAND_JOB_VIEW_DE

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| JobID                | Number          | true     | true     |                 |
| EmailID              | Number          | false    | false    |                 |
| AccountID            | Number          | false    | false    |                 |
| FromName             | Text(130)       | false    | false    |                 |
| FromEmail            | Text(100)       | false    | false    |                 |
| SchedTime            | Date            | false    | false    |                 |
| PickupTime           | Date            | false    | false    |                 |
| DeliveredTime        | Date            | false    | false    |                 |
| EventID              | Text(50)        | false    | false    |                 |
| JobType              | Text(50)        | false    | false    |                 |
| JobStatus            | Text(50)        | false    | false    |                 |
| EmailName            | Text(100)       | false    | false    |                 |
| EmailSubject         | Text(200)       | false    | false    |                 |
| SendClassification   | Text(36)        | false    | false    |                 |
| TACTIC_ID            | Text(50)        | false    | false    |                 |
