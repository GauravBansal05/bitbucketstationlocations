Dataextension name: TEMPLATIZED_PATIENT_INFO_DE

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| CUSTOMER_ID          | Text(50)        | true     | true     |                 |
| FIRST_NAME           | Text(50)        | false    | false    |                 |
| LAST_NAME            | Text(50)        | false    | false    |                 |
| EMAIL_ADDRESS        | EmailAddress    | false    | false    |                 |
