Dataextension name: demo

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| First Name           | Text(50)        | true     | false    |                 |
| email                | EmailAddress    | false    | false    |                 |
