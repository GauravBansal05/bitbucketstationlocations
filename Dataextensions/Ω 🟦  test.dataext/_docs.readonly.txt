Dataextension name: test

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| name                 | Text(50)        | true     | true     |                 |
