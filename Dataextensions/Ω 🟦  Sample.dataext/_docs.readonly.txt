Dataextension name: Sample

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| Firstname            | Text(50)        | false    | false    |                 |
| Lastname             | Text(50)        | false    | false    |                 |
| email                | EmailAddress    | false    | false    |                 |
