Dataextension name: Team3_Leads

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| First Name           | Text(50)        | false    | false    |                 |
| Last Name            | Text(50)        | false    | false    |                 |
| Email                | EmailAddress    | true     | true     |                 |
| Type                 | Text(50)        | false    | false    |                 |
| Membership           | Text(50)        | false    | false    |                 |
