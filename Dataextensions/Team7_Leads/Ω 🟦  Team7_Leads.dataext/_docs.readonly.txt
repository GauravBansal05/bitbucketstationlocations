Dataextension name: Team7_Leads

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| Email_Address        | EmailAddress    | true     | true     |                 |
| Name                 | Text(50)        | true     | false    |                 |
| Membership           | Text(50)        | false    | false    |                 |
| Type                 | Text(50)        | true     | false    |                 |
| Id                   | Text(50)        | false    | false    |                 |
