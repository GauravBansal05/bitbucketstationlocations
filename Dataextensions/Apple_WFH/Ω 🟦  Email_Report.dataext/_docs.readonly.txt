Dataextension name: Email_Report

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| Name                 | Text(100)       | false    | false    |                 |
| Email Address        | EmailAddress    | false    | false    |                 |
| Email Sent Time      | Date            | false    | false    |                 |
| Email Opens Count    | Number          | false    | false    |                 |
| Last Email Open Time | Date            | false    | false    |                 |
