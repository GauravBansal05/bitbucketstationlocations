Dataextension name: Entry Table

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| Name                 | Text(50)        | true     | false    |                 |
| Email                | EmailAddress    | true     | true     |                 |
| Type                 | Text(15)        | true     | false    |                 |
| Membership           | Text(20)        | true     | false    |                 |
