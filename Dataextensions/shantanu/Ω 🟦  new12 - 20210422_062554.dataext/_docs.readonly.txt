Dataextension name: new12 - 20210422_062554

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| First Name           | Text(50)        | false    | false    |                 |
| Last Name            | Text(50)        | false    | false    |                 |
| Phone                | Phone           | false    | false    |                 |
| Email                | EmailAddress    | true     | true     |                 |
| Address              | Text(50)        | false    | false    |                 |
| Status               | Text(50)        | false    | false    |                 |
