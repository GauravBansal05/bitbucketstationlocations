Dataextension name: MK_Person

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| ID                   | Text(50)        | true     | true     |                 |
