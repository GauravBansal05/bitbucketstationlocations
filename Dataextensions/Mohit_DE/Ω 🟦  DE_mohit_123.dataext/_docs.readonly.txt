Dataextension name: DE_mohit_123

| Name                 | Type            | Not NULL | PK       | Default Value   |
| -------------------- | --------------- | -------- | -------- | --------------- |
| Name                 | Text(50)        | true     | false    |                 |
| Email                | EmailAddress    | true     | false    |                 |
| Mobile No            | Number          | true     | false    |                 |
| Date of Birth        | Date            | true     | false    |                 |
